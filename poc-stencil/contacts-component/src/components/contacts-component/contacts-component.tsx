import { Component, State, Prop, Method } from "@stencil/core";

@Component({
  tag: 'contacts-component',
  styleUrl: 'contacts-component.css'
})
export class ContactsComponent {

  @Prop() maxLimit: number = 1000;
  @State() filteredList = [];
  originalList = [];

  async reloadContacts() {
    const response = await fetch(`http://localhost:3000/contacts`);
    if (response.status == 200) {
      this.originalList = [].slice.call(await response.json(), 0, this.maxLimit) || [];
      this.filteredList = this.originalList;
    }
  }

  @Method()
  filterByName(firstName: string) {
    if (firstName) {
      this.filteredList = this.originalList.filter(contact => 
        contact.first_name.toLowerCase().indexOf(firstName.toLowerCase()) >= 0)
    } else {
      this.filteredList = this.originalList;
    }    
  }

  componentWillLoad() {
    this.reloadContacts();
  }

  styles = {
    contactsTable: {
      display: 'table',
      fontFamily: 'sans-serif',
      fontSize: '9pt'      
    },
    row: {
      display: 'table-row',
    },
    header: {
      display: 'table-cell',
      fontWeight: 'bold',
      borderBottom: '1px solid gray',
      minWidth: '120px',
      padding: '2px 6px'
    },
    cell: {
      display: 'table-cell',
      padding: '2px 6px'
    }
  }

  render() {
    return (
      <div class="contacts-table" style={this.styles.contactsTable}>
        <div class="row" style={this.styles.row}>
          <span class="header" style={this.styles.header}>First Name</span>
          <span class="header" style={this.styles.header}>Last Name</span>
          <span class="header" style={this.styles.header}>Email</span>
          <span class="header" style={this.styles.header}>Gender</span>
        </div>
        {
          this.filteredList.map(contact => (
            <div class="row" style={this.styles.row}>
              <span class="cell" style={this.styles.cell}>{contact.first_name}</span>
              <span class="cell" style={this.styles.cell}>{contact.last_name}</span>
              <span class="cell" style={this.styles.cell}>{contact.email}</span>
              <span class="cell" style={this.styles.cell}>{contact.gender}</span>
            </div>
          ))
        }
      </div>
    )
  }
}