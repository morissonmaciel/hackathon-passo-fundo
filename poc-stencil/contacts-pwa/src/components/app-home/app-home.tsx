import { Component } from '@stencil/core';

@Component({
  tag: 'app-home',
  styleUrl: 'app-home.css'
})
export class AppHome {

  contactsComponent;

  doFilter(event) {
    const value = event.path[0].value;
    this.contactsComponent.filterByName(value);
  }

  render() {
    return (
      <div style={{ margin: '16px' }}>
        Filter by name: <input type="text" onInput={this.doFilter.bind(this)} />
        <br />
        <br />
        <contacts-component 
          ref={(instance) => this.contactsComponent = instance}
          maxLimit={50} />
      </div>
    );
  }
}
