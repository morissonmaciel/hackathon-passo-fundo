import React, { Component } from 'react';

import { BrowserRouter, Route, Switch } from "react-router-dom"

import './App.css';
import { TasksEdited } from './Tasks-Edit.js';
import { TaskList } from './components/TasksList/TaskList';
import { Home } from './components/Home/Home';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="div-app">
          <div className="div-tarefas">
            <TaskList />
          </div>
          <div className="div-rotas">

            <Switch>
              <Route path="/" exact component={Home}></Route>
              <Route path="/home" exact component={Home}></Route>
              {/* <Route path="/tasks" exact component={Tasks}></Route> */}
              <Route path="/cadastrar" component={TasksEdited}></Route>
              <Route path="/editar/:id" component={TasksEdited}></Route>
            </Switch>
        </div>

      </div>
      </BrowserRouter>
    );
  }
}

export default App;
