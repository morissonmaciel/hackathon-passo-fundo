import React, { Component } from "react";
import "./Home.css"

import plus from "../../imagens/plus.png";

export class Home extends Component {

    onClicarbotao() {
        this.props.history.push('/cadastrar')
    }

    render() {
        return (
            <div className='div-home'>
                <p>Bem-vindo ao meu sistema de tarefas</p>
                <p>Para iniciar, escolha uma das tarefas ao lado, ou crie uma nova no botão +</p>
                <button className="fab-criar" onClick={this.onClicarbotao.bind(this)}>
                    <img src={plus} className="fab-mais" alt="mais" />
                </button>
            </div>
        )
    }
}
