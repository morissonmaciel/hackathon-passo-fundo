import React, { Component } from 'react'

import { Link } from 'react-router-dom';
import "./TaskList.css";

import check from "../../imagens/check.png";

export class TaskList extends Component {
    state = {
        tasks: [{
            id: 1,
            titulo: 'tarefa 01',
            description: 'tarefa a fazer 1',
            concluido: false
        }, {
            id: 2,
            titulo: 'tarefa 02',
            description: 'tarefa a fazer 2',
            concluido: false
        },
        {
            id: 3,
            titulo: 'tarefa 03',
            description: 'tarefa a fazer 2',
            concluido: true
        }]
    }
    componentDidMount() {
        fetch('http://localhost:5000/tarefas') // consome api e poppula o array tasks
            .then((response) => response.json())
            .then((responseData) => {
                this.setState({ tasks: responseData.tarefas })
            });
    }
    render() {
        return (
            <div>
                <span className="title">Tarefas</span>
                {
                    this.state.tasks.map((tarefa) => (
                        <Link key={tarefa.id} to={`/editar/${tarefa.id}`}>
                        <div className="item-tarefa">
                            {
                                tarefa.concluida ? 
                                    <img className="imagem-check" src={check} alt="" />
                                :
                                <div className="marcador"></div>
                            }
                            <span>{tarefa.titulo}</span>
                        </div>
                        </Link>
                    ))
                }
            </div>
        )
    }
}

export default TaskList;