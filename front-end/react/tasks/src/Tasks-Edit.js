import React, { Component } from 'react'
import "./Tasks-Edit.css"

export class TasksEdited extends Component {

    state = {
        id: null,
        titulo: "",
        descricao: "",
        concluida: false
    }

    aoClicarBotaoCancelar() {
        this.props.history.push('/');
    }

    async aoClicarBotaoSalvar() {
        const tarefa = {
            id: this.state.id,
            titulo: this.state.titulo,
            descricao: this.state.descricao,
            concluida: this.state.concluida
        }
        const recurso = this.state.id !== null ? `/${this.state.id}` : "";
        const response = await fetch('http://localhost:5000/tarefas' + recurso,{
            method : this.state.id !== null ? "PUT" : "POST",
            headers : {"Content-Type":"application/json"},
            body : JSON.stringify(tarefa)
        });
        
        if (response.status === 200) {
            this.props.history.push('/');
        }
        else {
            const resposta = await response.json();
            alert(resposta)
        }
    }

    async aoClicarBotaoExcluir(id, event) {
        if (this.state.id === null) return;

        const recurso = this.state.id !== null ? `/${this.state.id}` : "";
        const response = await fetch('http://localhost:5000/tarefas' + recurso,{
            method : "DELETE",
        });
        
        if (response.status === 200) {
            this.props.history.push('/');
        }
        else {
            const resposta = await response.json();
            alert(resposta)
        }
    }

    aoEscreverTituloTarefa(event) {
        this.setState({
            titulo: event.target.value
        });
    }

    aoEscreverDescricaoTarefa(event) {
        this.setState({
            descricao: event.target.value
        });
    }

    aoAlterarCheckbox(event) {
        this.setState({
            concluida: event.target.value
        })
    }
    async consultarTarefa(id) {
        const response = await fetch(`http://localhost:5000/tarefas/${id}`);
        if (response.status === 200) {
            const json = await response.json()
            
            this.setState({
                id: id,
                titulo: json.tarefa.titulo,
                descricao: json.tarefa.descricao,
                concluida: json.tarefa.concluida
            })
        }

    }

    componentWillReceiveProps() {
        const { match } = this.props;
        const { params } = match != null ? match : null;
        const { id } = params != null ? params : null;
        id && this.consultarTarefa(id);
    }

    render() {
        return (
            <div className='div-edit'>
                <div className="div-titulo">Criar uma nova tarefa</div>
                <div className="div-form">
                    <form>
                        <table className="table-form">
                            <tbody>
                                <tr>
                                    <td>Título da Tarefa:</td>
                                    <td>
                                        <input type="text" className='caixasDeTexto' onChange={this.aoEscreverTituloTarefa.bind(this)} value={this.state.titulo} />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Descrição:</td>
                                    <td>
                                        <textarea rows="3" cols="50" className="caixasDeTexto" onChange={this.aoEscreverDescricaoTarefa.bind(this)} value={this.state.descricao} />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Concluída:</td>
                                    <td>
                                        <input type="checkbox" checked={this.state.concluida} onChange={this.aoAlterarCheckbox.bind(this)}/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
                <div className="div-expandida"></div>
                <div className="div-botoes">
                {
                    this.state.id ?
                    <button className='botao-form botaoExcluir' onClick={this.aoClicarBotaoExcluir.bind(this)}>Excluir</button>
                    :
                    null
                }
                    <span style={{ flex: 1 }}></span>
                    <button className='botao-form botaoCancelar' onClick={this.aoClicarBotaoCancelar.bind(this)}>Cancelar</button>
                    <button className='botao-form botaoSalvar' onClick={this.aoClicarBotaoSalvar.bind(this)}>Salvar</button>
                </div>
            </div>
        )
    }
}