from flask import jsonify
from flask import request
from tarefas_logica.model import TarefaSchema
from tarefas_logica.logica import TarefaLogica

tarefa_logica = TarefaLogica()
tarefa_schema = TarefaSchema()


def tarefa_create():
    data, errors = tarefa_schema.load(request.get_json())
    if errors:
        return jsonify({"message": '{}'.format(errors)}), 401

    retorno = tarefa_logica.create_tarefa(data)

    if(retorno):
        return jsonify({"id": retorno}), 200
    else:
        return jsonify({"message": "Não foi possivel criar a tarefa"}), 401


def delete_tarefa(id):
    retorno = tarefa_logica.delete_tarefa(id)

    if(retorno) is True:
        return jsonify({'mensagem': "Tarefa Excluida com sucesso"}), 200
    else:
        return jsonify({"message": "Erro ao excluir tarefa"}), 401


def get_tarefas():
    retorno = tarefa_logica.get_all_tarefas()

    if not (retorno):
        return jsonify({"message": "Não há tarefas cadastradas"}), 401
    else:
        return jsonify({"tarefas": retorno}), 200


def get_tarefas_by_id(id):
    retorno = tarefa_logica.get_tarefa_by_id(id)

    if(retorno) is False:
        return jsonify({"message": "Tarefa não encontrada"}), 404
    else:
        return jsonify({"tarefa": retorno}), 200


def set_tarefa_name(id):
    data, errors = tarefa_schema.load(request.get_json())
    if errors:
        return jsonify({"message": '{}'.format(errors)}), 401

    retorno = tarefa_logica.updateById(data, id)

    if(retorno):
        return jsonify({"message": "Tarefa atualizada com sucesso"}), 200
    else:
        return jsonify(
            {"message": "Não foi encontrada uma tarefa com o id desejado"}
            ), 401
