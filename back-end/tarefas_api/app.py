from flask import Flask
from tarefas_api.tarefaAPI import (
    tarefa_create,
    get_tarefas,
    get_tarefas_by_id,
    set_tarefa_name,
    delete_tarefa)
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

app.add_url_rule('/tarefas', view_func=tarefa_create, methods=['POST'])

app.add_url_rule('/tarefas', view_func=get_tarefas, methods=['GET'])

app.add_url_rule('/tarefas/<id>', view_func=get_tarefas_by_id, methods=['GET'])

app.add_url_rule('/tarefas/<id>', view_func=set_tarefa_name, methods=['PUT'])

app.add_url_rule('/tarefas/<id>', view_func=delete_tarefa, methods=['DELETE'])

if __name__ == "__main__":
    app.run(port=5000)
