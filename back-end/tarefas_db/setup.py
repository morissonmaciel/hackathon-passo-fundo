from setuptools import setup, find_packages

setup(
    name='tarefas_db',
    version='1.0.0',
    packages=find_packages(include=['tarefas_db']),
    install_requires=[
        'pymongo==3.7.2'
    ]
)