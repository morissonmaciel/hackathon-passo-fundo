from pymongo import MongoClient

class TarefasDbAdapter:
    @classmethod
    def createConnection(self):
        mongo = MongoClient('mongodb://127.0.0.1:27017/restdb')
        db = mongo.db.tarefas
        self.db = db
