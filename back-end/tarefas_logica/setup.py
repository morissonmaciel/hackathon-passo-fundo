from setuptools import setup, find_packages

setup(
    name='tarefas_logica',
    version='1.1.0',
    install_requires=[
        'marshmallow==2.16.3',
        'tarefas_db==1.0.0'
    ],
    packages=find_packages(include=['tarefas_logica'])
)