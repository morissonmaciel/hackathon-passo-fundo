from unittest import TestCase
from tarefas_logica.logica import TarefaLogica
from tarefas_logica.model import TarefaSchema


class TarefasTest(TestCase):
    def setUp(self):
        return

    def tearDown(self):
        return

    def test_create_tarefa(self):
        adapter_logica = TarefaLogica()

        payload = {
            "titulo": "Comprar Pão",
            "descricao": "Comprar o pão de manhã logo cedo",
            "concluida": False
        }

        instancia, error = TarefaSchema().load(payload)
        self.assertEqual(error, {})

        tarefa_id = adapter_logica.create_tarefa(instancia)
        self.assertNotEqual(tarefa_id, None)
        self.assertNotEqual(tarefa_id, False)

    def test_create_tarefa_erro_validacao(self):

        payload = {
            "titulo": "Comprar Pão",
            "descricao": "Comprar o pão de manhã logo cedo",
            # "concluida": False
        }

        tarefa_schema = TarefaSchema().load(payload)
        self.assertNotEqual(tarefa_schema.errors, {})

    def test_get_tarefa_by_id_not_null(self):
        adapter_logica = TarefaLogica()
        instancia = {
            "titulo": "Comprar Pão",
            "descricao": "Comprar o pão de manhã logo cedo",
            "concluida": False
        }
        tarefa_id = adapter_logica.create_tarefa(instancia)
        retorno = adapter_logica.get_tarefa_by_id(tarefa_id)
        self.assertNotEqual(retorno, None)

    def test_get_tarefa_by_id_null(self):
        adapter_logica = TarefaLogica()
        retorno = adapter_logica.get_tarefa_by_id("abc")
        self.assertEqual(retorno, None)

    def test_updateById(self):
        adapter_logica = TarefaLogica()
        instancia = {
            "titulo": "Comprar Pão",
            "descricao": "Comprar o pão de manhã logo cedo",
            "concluida": False
        }
        tarefa_id = adapter_logica.create_tarefa(instancia)
        self.assertNotEqual(tarefa_id, None)
        self.assertNotEqual(tarefa_id, False)

        new_instancia = {
            "titulo": "Comprar Mel",
            "descricao": "Comprar o Mel de tarde na noitinha",
            "concluida": True
        }
        retorno = adapter_logica.updateById(
            new_instancia, tarefa_id)
        self.assertEqual(retorno, True)

        tarefa_modificada = adapter_logica.get_tarefa_by_id(tarefa_id)
        self.assertNotEqual(tarefa_modificada, None)
        self.assertEqual(
            tarefa_modificada["concluida"], new_instancia["concluida"])

    def test_delete_tarefa(self):
        adapter_logica = TarefaLogica()
        instancia = {
            "titulo": "Comprar Pão",
            "descricao": "Comprar o pão de manhã logo cedo",
            "concluida": False
        }
        tarefa_id = adapter_logica.create_tarefa(instancia)
        self.assertNotEqual(tarefa_id, None)
        self.assertNotEqual(tarefa_id, False)

        retorno = adapter_logica.delete_tarefa(
            tarefa_id)
        self.assertEqual(retorno, True)
