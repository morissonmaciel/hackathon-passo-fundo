from marshmallow import fields, Schema


class TarefaSchema(Schema):
    id = fields.Str(allow_none=True)
    titulo = fields.Str(allow_none=False, required=True)
    descricao = fields.Str(allow_none=True)
    concluida = fields.Bool(allow_none=False, required=True)
