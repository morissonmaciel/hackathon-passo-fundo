from bson.objectid import ObjectId
from tarefas_db.db_client import TarefasDbAdapter


class TarefaLogica():

    db_adapter = TarefasDbAdapter()

    def __init__(self):
        self.db_adapter.createConnection()

    @classmethod
    def create_tarefa(self, tarefa_dict):
        tarefa = self.db_adapter.db.insert_one(tarefa_dict)
        if(tarefa.inserted_id.__str__()) is not None:
            retorno = tarefa.inserted_id.__str__()
        else:
            retorno = False

        return retorno

    @classmethod
    def delete_tarefa(self, id):
        self.db_adapter.db.delete_one({'_id': ObjectId(id)})
        tarefas_obj = self.db_adapter.db.find_one({'id': id})
        retorno = False

        if tarefas_obj is not None:
            retorno = False
        else:
            retorno = True
        return retorno

    @classmethod
    def get_all_tarefas(self):
        retorno = []
        try:
            for tarefa_obj in self.db_adapter.db.find():
                id = tarefa_obj['_id'].__str__()
                retorno.append(
                    {
                        'id': id,
                        'titulo': tarefa_obj['titulo'],
                        'descricao': tarefa_obj['descricao'],
                        'concluida': tarefa_obj['concluida']
                    })
        except BaseException as error:
            print(error)
        return retorno

    @classmethod
    def get_tarefa_by_id(self, id):
        try:
            tarefas_obj = self.db_adapter.db.find_one({'_id': ObjectId(id)})
            tarefas_obj.pop('_id')
            return tarefas_obj
        except BaseException as error:
            print(error)
            return None

    @classmethod
    def updateById(self, req, id):
        retorno = self.db_adapter.db.update_one(
            {"_id": ObjectId(id)},
            {
                "$set": {
                    "titulo": req['titulo'],
                    "descricao": req['descricao'],
                    "concluida": req['concluida']
                }
            }
        )

        if(retorno.modified_count > 0):
            return True
        else:
            return False
