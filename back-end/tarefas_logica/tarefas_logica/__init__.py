from .logica import TarefaLogica
from .model import TarefaSchema

__all__ = ['TarefaLogica', 'TarefaSchema']
