from flask import Flask
from flask import jsonify
from flask import request
from flask import current_app as app
from flask_pymongo import PyMongo
from tarefas_logica.tarefas import Tarefas
from pymongo import MongoClient
from flask_cors import CORS
from tarefas_db.db_client import TarefasDbAdapter
from tarefas_logica.tarefa import TarefaSchema

app = Flask(__name__)
CORS(app)

adapter = TarefasDbAdapter()
adapter.createConnection()
db = adapter.db.tarefas

tarefa = Tarefas()

@app.route('/tarefas', methods=['POST'])
def tarefa_create():
    tarefa_schema = TarefaSchema().load(request.get_json())
    # request_tarefa = request.get_json()
    retorno = tarefa.create_tarefa(tarefa_schema, db)

    if(retorno) != False:
        return jsonify({"id": retorno})
    else:
        return jsonify({"message" : "Não foi possivel criar a tarefa"})

@app.route('/tarefas/<id>', methods=['DELETE'])
def delete_tarefa(id):
    retorno = tarefa.delete_tarefa(id, db)

    if(retorno) == True:
        return jsonify({'mensagem': "Tarefa Excluida com sucesso"})
    else:
        return jsonify({"message": "Erro ao excluir tarefa"})

@app.route('/tarefas', methods=['GET'])
def get_tarefas():
    retorno = tarefa.get_all_tarefas(db)

    if(retorno) == False:
        return jsonify({"message": "Não há tarefas cadastradas"})
    else:
        return jsonify({"tarefas": retorno})

@app.route('/tarefas/<id>', methods=['GET'])
def get_tarefas_by_id(id):
    retorno = tarefa.get_tarefa_by_id(id, db)

    if(retorno) == False:
        return jsonify({"message": "Tarefa não encontrada"}), 404
    else:
        return jsonify({"tarefa": retorno}), 200

@app.route('/tarefas/<id>', methods=['PUT'])
def set_tarefa_name(id):
    request_data = request.get_json()   
    retorno = tarefa.updateById(request_data, id, db)

    if(retorno):
        return jsonify({"message": "Tarefa atualizada com sucesso"})
    else:
        return jsonify({"message": "Não foi encontrada uma tarefa com o id desejado"})

if __name__ == "__main__":
    app.run(port=5000)